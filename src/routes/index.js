const express = require('express');
const userRouter = require('../user/user.routes');
const router = express.Router();


router.get('/status', (req, res) => {
    return res.send("APP running")
})

router.use('/users', userRouter);


module.exports = router;