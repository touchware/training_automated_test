const mongoose = require("mongoose");
const environment = require("./vars");

mongoose.Promise = Promise;

// Exit application on error
mongoose.connection.on("error", err => {
    console.error(`MongoDB connection error: ${err}`);
    throw new Error('Error connecting to the db')
});

const mongooseConfig = {
    connect: () => {
        mongoose.connect(
            environment.mongo_uri,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true
            }
        );
        return mongoose.connection;
    }
};

module.exports = mongooseConfig
