require('dotenv').config()

const environment = {
    environment: process.env.NODE_ENV,
    port: process.env.PORT,
    mongo_uri: process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TEST : process.env.MONGO_URI
}

module.exports = environment