const express = require('express');
const bodyParser = require('body-parser');
const router = require('../routes');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use('/api', router);


app.use((req, res, next) => {
    const error = new Error();
    error.status = 404;
    error.message = 'Route not found'
    next(error);
})

app.use((err, req, res, next) => {
    if (err.status === 404) {
        return res.status(err.status).json({
            message: err.message
        })
    }
    return res.status(500).json({
        message: "Internal server error"
    })
})

module.exports = app