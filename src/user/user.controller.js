const { validUserPost } = require("./user.validation");
const UserService = require('./index')
const UserController = {
    saveUser: async (req, res) => {
        const { firstName, lastName } = req.body;
        //Validate the user
        const errors = validUserPost({ firstName, lastName })
        if (errors && errors.length > 0) {
            return res.status(400).json({
                errors
            })
        }
        //Save the user to database
        try {
            const user = await UserService.save({ firstName, lastName })
            return res.status(200).json({
                user
            })

        } catch (error) {
            return res.status(500).json({
                message: "Internal server error"
            })
        }


    }
}

module.exports = UserController