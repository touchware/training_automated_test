const express = require('express');
const UserController = require('./user.controller');
const userRouter = express.Router();

userRouter.post('/', UserController.saveUser)

module.exports = userRouter;