const UserService = require('./user.service')
const UserModel = require('./user')

module.exports = new UserService(UserModel)