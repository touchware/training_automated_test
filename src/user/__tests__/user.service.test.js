const UserService = require("../user.service")
const sinon = require('sinon');

describe('User service test', () => {
    describe('Save user test', () => {
        it('should call the create method in the model', async () => {
            const UserModelMock = {
                create: sinon.spy()
            }
            const userService = new UserService(UserModelMock)
            await userService.save({})
            expect(UserModelMock.create.calledOnce).toBeTruthy()
        })
    })
})