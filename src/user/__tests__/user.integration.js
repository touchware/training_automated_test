const app = require('../../config/express')
const mongoose = require('mongoose');
const mongooseConfig = require('../../config/database')
const request = require('supertest');
const faker = require('faker')
describe('App integration test', () => {
    let server;

    beforeAll(async () => {
        server = await app.listen(3001)
        await mongooseConfig.connect()

    })

    afterAll(async () => {
        const collections = mongoose.connection.collections;

        for (const key in collections) {
            const collection = collections[key];
            await collection.deleteMany({});
        }
        await server.close();
        await mongoose.connection.close();
    })

    describe('Save user test', () => {
        it('should give me 200 if firstName and lastName is passed', async () => {
            const data = {
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName()
            }
            const response = await request(server).post('/api/users').send(data).expect(200)
            expect(response.body.user._id).toBeDefined()
        })

        it('should give me 400 if firstName  is not passed', async () => {
            const data = {
                lastName: faker.name.lastName()
            }
            await request(server).post('/api/users').send(data).expect(400)
        })
    })

})