const { validUserPost } = require("../user.validation")
const faker = require('faker')

describe("User validation test", () => {
    describe("Valid User post test", () => {
        it('Should return array of validation error if firstName and lastName is not passed', () => {
            const errors = validUserPost({});
            expect(errors.length).toBe(2);
        })
        it('Should return array of validation error if firstName is not passed', () => {
            const errors = validUserPost({ lastName: faker.name.lastName() });
            expect(errors.length).toBe(1);
            expect(errors[0].field).toBe('firstName')
        })

        it('Should return array of validation error if lastName is not passed', () => {
            const errors = validUserPost({ firstName: faker.name.firstName() });
            expect(errors.length).toBe(1);
            expect(errors[0].field).toBe('lastName')
        })

        it('Should return empty array of validation error if both firstName and lastName is  passed', () => {
            const errors = validUserPost({ firstName: faker.name.firstName(), lastName: faker.name.lastName() });
            expect(errors.length).toBe(0);
        })
    })
})