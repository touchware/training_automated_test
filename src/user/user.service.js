class UserService {
    constructor(UserModel) {
        this.UserModel = UserModel
    }

    async save({ ...data }) {
        try {
            const user = await this.UserModel.create(data);
            return user;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = UserService