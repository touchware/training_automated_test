const validUserPost = ({ ...params }) => {
    const { firstName, lastName } = params;
    let errors = []
    if (!firstName || firstName === '') {
        errors = [...errors, {
            type: 'Validation Error',
            field: 'firstName',
            message: "First name is required"
        }]
    }

    if (!lastName || lastName === '') {
        errors = [...errors, {
            type: 'Validation Error',
            field: 'lastName',
            message: "Last name is required"
        }]
    }

    return errors
}

module.exports = { validUserPost }

