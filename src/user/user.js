const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: "First name is required"
    },
    lastName: {
        type: String,
        required: "Last Name is required",
        index: true
    }
})

const User = mongoose.model('User', UserSchema);

module.exports = User;