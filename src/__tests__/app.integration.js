const app = require('../config/express')
const request = require('supertest');
describe('App integration test', () => {
    let server;

    beforeAll(async () => {
        server = await app.listen(3001)
    })

    afterAll(async () => {
        await server.close()
    })

    describe('Router is mounted properly', () => {
        it('should give me 200 status for /status route', async () => {
            await request(server).get('/api/status').expect(200)
        })

        it('should give me 404 status for /404 route', async () => {
            await request(server).get('/api/404').expect(404)
        })
    })

})